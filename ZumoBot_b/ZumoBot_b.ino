
#include <Wire.h>
#include <LSM303.h>
#include "ZumoAitor.h"
#include <ZumoMotors.h>
#include <Pushbutton.h>

//Ultrasonic ultrasonic(2,5,3000); // (Trig PIN,Echo PIN)

#define useMotors true

#define TIME_WAIT_SHAKES_MS 1000

#define ACCEL_FACTOR_REDUCTION 500
#define ACCEL_THESHOLD 5
#define DEBUG false
//#define select0 11
//#define select1 2
#define servoPin 5
#define analogPortLeft 3
#define analogPortCenter 4
#define analogPortRight 5
#define analogPortLeftSonar 0
#define analogPortCenterSonar 1
#define analogPortRightSonar 2

#define minSonarRead 200

#define centeredTimeAfterTurn 1200

int r0;
int r1;
int count;

int pinValues[3];
int pinWhiteThreshold[3];
boolean pinWhite[3];
boolean validWhitePin[3];
boolean pinSonar[3];

LSM303 compass;
ZumoMotors motors;

char report[80];

int idleAccelX = 0;
int idleAccelY = 0;

long lastShakeReceived = 0;

long timeInterval;

long lastStateChange = 0;

long nextStateChange = 0;
long nextSemiStateChange = 0;

int currentState = BUSCANDO;

int shakeDirection = NO;

#define SEARCH_TARGET_EX_PERIOD 30

//fases, 0 inicio, 1 buscando, 3 end
int sonarPhase = 0;


Pushbutton button(ZUMO_BUTTON);
void setup()
{
  pinMode(13, OUTPUT);
  pinMode(6, OUTPUT);
  digitalWrite(13, HIGH);
  motors.flipRightMotor(true);
  motors.flipLeftMotor(true);



  Serial.begin(115200);
  digitalWrite(13, LOW);

  Wire.begin();
  compass.init();
  compass.enableDefault();

  delay(500);
  compass.read();

  idleAccelX = compass.a.x / ACCEL_FACTOR_REDUCTION;
  idleAccelY = compass.a.y / ACCEL_FACTOR_REDUCTION;

//  while (true) {
//    //updateAnalogValues();
//    updateSonar();
//    delay(1000);
//  }
  digitalWrite(13, HIGH);
  button.waitForButton();
  initValues(true);
  delay(100);
  digitalWrite(13, LOW);
  button.waitForButton();
  initValues(false);
  delay(100);
  digitalWrite(13, HIGH);
  button.waitForButton();
  long time = millis();
  while (millis() - time < 5000) {
  }
}


// 1 - escaneamos linea blanca, máxima prioridad.
// 2 - escaneamos posible golpe.
// 3 - escaneamos posible detección Sonar.
// 4 - tomamos acción en función de estado actual e posibles accions pendentes.

void loop()
{
  //1 - escanear linea TODO
  digitalWrite(13, LOW);
  updateAnalogValues();
  boolean white = checkWhite();
  //white = false;
  updateSonar();




  if (white && currentState != EVASION_LINEA) {
    //se non estamos evadindo linea.. pois haina que evadir.
    int tempoAddPorCentro = pinWhite[1] ? 65 : 45;
    if (pinWhite[0]) { //izquierda -> viramos dereita
      giroQuieto(true, tempoAddPorCentro);
    } else if (pinWhite[2]) { //derecha
      giroQuieto(false, tempoAddPorCentro);
    } else { //centro
      giroQuieto(true, tempoAddPorCentro);
    }
    setState(EVASION_LINEA);
    //despois dese tempo, cambiamos a avanzar lentos con algun xiro
  }

  if (currentState == EVASION_LINEA && millis() > nextStateChange) {
    avanzar(false, 50);
    setState(BUSCANDO);
  }

  //2  - escanear sensor aceleración
  analizarAceleracion();
  if (shakeDirection != NO && currentState == BUSCANDO) {
    if (DEBUG) {
      printChoque(shakeDirection);
    }
    //evasión EVASION_GOLPE
    if (shakeDirection & ESQUERDA) {
      giroMarcha(true, true, 70, true);
      setState(EVASION_GOLPE);
    } else if (shakeDirection & DEREITA) {
      giroMarcha(false, true, 70, true);
      setState(EVASION_GOLPE);
    }
  }

  if (currentState == EVASION_GOLPE && millis() > nextStateChange) {
    avanzar(false, 40);
    setState(BUSCANDO);
  }

  if (currentState == ATACANDO && millis() > nextStateChange ) {
    //si sigue
    if (isCenterTarget()) {
      avanzar(true, 30);
      setState(ATACANDO);
      analogWrite(6, 10);
      delay(10);
      digitalWrite(6, LOW);
    } else {
      setState(BUSCANDO);
    }
  }

  if (currentState == BUSCANDO && isCenterTarget()) {
    avanzar(true, 30);
    //avanzar a topeeee ATACANDO
    setState(ATACANDO);
    analogWrite(6, 80);
    delay(30);
    digitalWrite(6, LOW);
  }

  int target = thereIsTarget();
  if (currentState == BUSCANDO  && target != 0 &&  millis() > nextSemiStateChange) {
    boolean dereita = target == 1;
    boolean fast = true;
    if (DEBUG) {
      Serial.print("objetivo en dereita? ");
      Serial.println(dereita);
    }
    int gireTime = 30; //FIXME
    giroMarcha(dereita, true, gireTime, true);
    nextSemiStateChange = millis() + gireTime;
  }

  //si llegados a este punto estamos BUSCANDO.. hacemos algun giro
  if (currentState == BUSCANDO  && millis() > nextStateChange) {
    int rand = random(10);
    boolean dereita = rand >= 5;
    giroMarcha(dereita, true, 300, false);
  }
  digitalWrite(13, HIGH);
  // servo.write(90);
}

boolean isCenterTarget() {
  return pinSonar[1];
}

int thereIsTarget() {
  if (pinSonar[2]) {
    //objetivo dereita
    return 1;
  } else if (pinSonar[0]) {
    return -1; //izquierda
  }
  return 0;
}

//END LOOP
void avanzar(boolean rapido, int tempo) {
  int velocidad = rapido ? 400 : 350;
  if (useMotors) {
    motors.setLeftSpeed(velocidad);
    motors.setRightSpeed(velocidad);
  }
  nextStateChange = millis() + (tempo * 10);
}

void giroQuieto(boolean dereita, int tempo) {
  int multiplier = dereita ? 1 : -1;
  if (useMotors) {
    motors.setLeftSpeed(340 * multiplier);
    motors.setRightSpeed(-300 * multiplier);
  }
  nextStateChange = millis() + (tempo * 10);
}

void giroMarcha(boolean dereita, boolean adiante, int tempo, boolean giroRapido) {
  int direccionMultiplier = adiante ? 1 : -1;
  int vDereita = dereita ? (giroRapido ? -200 : 10) : (giroRapido ? 400 : 390);
  int vEsquerda = dereita ? (giroRapido ? 390 : 390) : (giroRapido ? -200 : 10);
  if (useMotors) {
    motors.setLeftSpeed(vEsquerda * direccionMultiplier);
    motors.setRightSpeed(vDereita * direccionMultiplier);
  }
  nextStateChange = millis() + (tempo * 10);
}


void analizarAceleracion() {
  compass.read();
  int accelX = (compass.a.x / ACCEL_FACTOR_REDUCTION) - idleAccelX;
  int accelY = (compass.a.y / ACCEL_FACTOR_REDUCTION) - idleAccelY;

  shakeDirection = NO;

  if (accelY > ACCEL_THESHOLD) {
    shakeDirection |= DEREITA;
  }
  else if (-accelY > ACCEL_THESHOLD) {
    shakeDirection |= ESQUERDA;
  }
  if (accelX > ACCEL_THESHOLD) {
    shakeDirection |= ATRAS;
  }
  else if (-accelX > ACCEL_THESHOLD) {
    shakeDirection |= ADIANTE;
  }
  if (shakeDirection != NO) {
    if ((millis() - lastShakeReceived) < TIME_WAIT_SHAKES_MS) {
      shakeDirection = NO;
      return;
    }
    lastShakeReceived = millis();
  }
  //atras X +
  //dereita Y +
}

//exigimos que entre golpes esperemos al menos 1 segundo para procesar el siguiente
void printChoque(int shakeDirection) {
  if (shakeDirection != NO) {
    Serial.print("golpe:");
    if (shakeDirection & ADIANTE) {
      Serial.print(" ADIANTE ");
    }
    else if (shakeDirection & ATRAS) {
      Serial.print(" ATRAS ");
    }

    if (shakeDirection & ESQUERDA) {
      Serial.print(" ESQUERDA ");
    }
    else if (shakeDirection & DEREITA) {
      Serial.print(" DEREITA ");
    }
    else {
      Serial.print(" CENTRO ");
    }
    Serial.println();
  }
}


void updateAnalogValues() {
  for (count = 0; count <= 2; count++) {
    //r0 = bitRead(count,0);
    //r1 = bitRead(count,1);
    //digitalWrite(select0, r0);
    //digitalWrite(select1, r1);
    //delayMicroseconds(1000); //Non estou seguro de se fai falta, pero por se acaso. Para que o 4051 teña tempo a actualizar a salida.
    pinValues[count] = (analogRead(analogPortLeft + count) + analogRead(analogPortLeft + count)) / 2;

    //    Serial.print(count);
    //    Serial.print(" - ");
    //    Serial.println(pinValues[count]);
  }
}

void isort(int *a, int n) {
  // *a is an array pointer function
  for (int i = 1; i < n; ++i)
  {
    int j = a[i];
    int k;
    for (k = i - 1; (k >= 0) && (j < a[k]); k--)
    {
      a[k + 1] = a[k];
    }
    a[k + 1] = j;
  }
}


//first = negro
//!first = blanco
//en primera fase se leen los valores negros, en segunda se lee blanco y se asume el punto medio como corte.
void initValues(boolean first) {
  updateAnalogValues();
  for (count = 0; count <= 2; count++) {
    if (first) {
      pinWhiteThreshold[count] = pinValues[count];
      Serial.print(count);
      Serial.print(" Negro - ");
      Serial.println(pinValues[count]);
    } else {
      if (pinWhiteThreshold[count] - pinValues[count] > 100) {
        validWhitePin[count] = true;
      } else {
        validWhitePin[count] = false;
        Serial.print("pin invalido por no superar 100 de dif - ");
      }
      pinWhiteThreshold[count] = (
                                   pinWhiteThreshold[count] + pinValues[count]) / 2;
      Serial.print(count);
      Serial.print(" Blanco - ");
      Serial.print(pinValues[count]);
      Serial.print(" - ");
      Serial.println(pinWhiteThreshold[count]);



    }



  }
}

boolean checkWhite() {
  boolean retunValue = false;
  for (count = 0; count <= 2; count++) {
    if (pinValues[count] < pinWhiteThreshold[count] && validWhitePin[count]) {
      if (DEBUG) {
        Serial.print("White on");
        Serial.print(count);
        Serial.print(" - ");
        Serial.println(pinValues[count]);
      }
      pinWhite[count] = true;
      retunValue = true;
    } else {
      pinWhite[count] = false;
    }
  }
  return retunValue;
}

void setState(int STATE) {
  Serial.print("setState ");
  Serial.println(STATE);
  currentState = STATE;
  lastStateChange = millis();
}

#define sonarCounts 5

void updateSonar() {
  int currentSonarValues[3][sonarCounts];
  for (count = 0; count < sonarCounts; count++) {
    for (int i = 0; i < 3; i++) {
      currentSonarValues[i][count] = analogRead(i);
      delay(1);
    }
  }
  for (int i = 0; i < 3; i++) {
    isort(currentSonarValues[i], sonarCounts);
    pinSonar[i] = currentSonarValues[i][2] > minSonarRead;
    if (DEBUG) {
      Serial.print("sonar ");
      Serial.print(i);
      Serial.print(" ");
      Serial.println(pinSonar[i]);
    }
  }


}

boolean cycleCheck(unsigned long *lastMillis, unsigned int cycle)
{
  unsigned long currentMillis = millis();
  if (currentMillis - *lastMillis >= cycle)
  {
    *lastMillis = currentMillis;
    return true;
  }
  else
    return false;
}

