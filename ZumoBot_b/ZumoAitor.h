
//Constantes para choques, para saber de onde ven
#define NO 0x00
#define ATRAS 0x01
#define ADIANTE 0x02
#define ESQUERDA 0x04
#define DEREITA 0x08
#define CENTRO 0x16

//Estados
#define INICIO 0x01
#define BUSCANDO 0x02
#define ATACANDO 0x03
#define EVASION_GOLPE 0x04
#define EVASION_LINEA 0x05
